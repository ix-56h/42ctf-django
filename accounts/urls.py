from django.urls import path
from . import views

app_name = "accounts"

urlpatterns = [
        path('signin/', views.signin, name='signin'),
        path('signup/', views.signup, name='signup'),
        path('profile/<str:user_name>', views.profile, name='profile'),
        path('account/', views.account, name='account'),
        path('logout/', views.out, name='out')
]
