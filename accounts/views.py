from django.shortcuts import render
from django import forms
from .forms import UserForm,UserProfileInfoForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

def signin(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request,user)
                    return HttpResponseRedirect(reverse('home'))
                else:
                    return HttpResponse("Your account was inactive.")
            else:
                return render(request, 'accounts/login.html', {'error': True})
        else:
            return render(request, 'accounts/login.html', {})
    else:
        return HttpResponseRedirect(reverse('home'))

def signup(request):
    if not request.user.is_authenticated:
        user_form = UserForm()
        profile_form = UserProfileInfoForm()
        registered = False
        if request.method == 'POST':
            pass1 = request.POST.get('password')
            if len(pass1) < 8:
                return render(request,'accounts/register.html', {'user_form':user_form, 'profile_form':profile_form, 'registered_failed':"The new password must be at least %d characters long." % 8})
            first_isalpha = pass1[0].isalpha()
            if not any(c.isdigit() for c in pass1) or not any(c.isalpha() for c in pass1):
                return render(request,'accounts/register.html', {'user_form':user_form, 'profile_form':profile_form, 'registered_failed':"The password must contain at least one letter and at least one digit or punctuation character."})
            if User.objects.filter(email=request.POST.get('email')).exists():
                return render(request,'accounts/register.html', {'user_form':user_form, 'profile_form':profile_form, 'registered_failed':"A user with that email already exists."})
            user_form = UserForm(data=request.POST)
            profile_form = UserProfileInfoForm(data=request.POST)
            if user_form.is_valid() and profile_form.is_valid():
                user = user_form.save()
                user.set_password(user.password)
                user.save()
                profile = profile_form.save(commit=False)
                profile.user = user
                profile.save()
                registered = True
            else:
                return render(request,'accounts/register.html', {'user_form':user_form, 'profile_form':profile_form, 'registered_failed':"A user with that username already exists."})
        return render(request,'accounts/register.html',
                {'user_form':user_form,
                    'profile_form':profile_form,
                    'registered':registered})
    else:
        return HttpResponseRedirect(reverse('home'))

@login_required
def out(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))

@login_required
def account(request):
    pass

@login_required
def profile(request):
    pass

# Create your views here.
