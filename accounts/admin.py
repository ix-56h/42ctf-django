from .models import UserProfileInfo
from django.contrib import admin

admin.site.register(UserProfileInfo)
# Register your models here.
