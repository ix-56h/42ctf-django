from django.shortcuts import render
from django.http import HttpResponse
from .models import new
from ctfs.models import Category, CTF
from accounts.models import UserProfileInfo

def home(request):
    news        =   new.objects.order_by('-pub_date')[:5]
    latest_ctfs =   CTF.objects.order_by('-pub_date')[:5]
    top10       =   UserProfileInfo.objects.select_related().order_by('-score', 'last_submission_date', 'user__username')[:10]
    return render(request, 'home/home.html', {'news' : news, 'ctfs' : latest_ctfs, 'top' : top10})

# Create your views here.
